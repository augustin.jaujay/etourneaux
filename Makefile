EXECUTABLE := build/etourneaux
SRC_DIR := src
OBJ_DIR := build/obj
SRC := $(wildcard $(SRC_DIR)/*.cpp $(SRC_DIR)/*/*.cpp)
OBJ := $(patsubst $(SRC_DIR)/%.cpp, $(OBJ_DIR)/%.o, $(SRC))
FLAGS := -Wall
LIBRARIES := -lSDL2

compile: $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJ)
	g++ $(FLAGS) -o $@ $^ $(LIBRARIES)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	mkdir -p $(@D)
	g++ -c -o $@ $<

run: compile
	./$(EXECUTABLE)

clean:
	rm -rf build/