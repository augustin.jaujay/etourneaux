#ifndef INDIVIDUAL_HPP_INCLUDED
#define INDIVIDUAL_HPP_INCLUDED

#include <vector>

class Individual {
    private:

    int number;
    int x, y;
    int width, height;
    int heading;

    int distance(Individual otherIndividual);
    bool isNearBorder(int border);
    bool onWhichSideIsIndividual(Individual otherIndividual);

    public:

    Individual(int argNumber, int argWidth, int argHeight);
    int getX();
    int getY();
    int getHeading();
    void setSize(int w, int h);
    void step(std::vector<Individual> individuals);
};

#endif // INDIVIDUAL_HPP_INCLUDED