#include "individual.hpp"
#include <math.h>
#include <iostream>

#define BORDER 0
#define SIGHT 100
#define DATTRACTION 70
#define DREPULSION 20

Individual::Individual(int argNumber, int argWidth, int argHeight) : 
    number(argNumber),
    width(argWidth),
    height(argHeight)
    {
    x = std::rand() % width;
    y = std::rand() % height;
    heading = std::rand() % 360;
}

int Individual::getX() {
    return x;
}

int Individual::getY() {
    return y;
}

int Individual::getHeading() {
    return heading;
}

void Individual::setSize(int w, int h) {
    width = w;
    height = h;
}

void Individual::step(std::vector<Individual> individuals) {
    std::vector<Individual> closeIndividuals;
    for (int i = 0; i < individuals.size(); i++) {
        Individual currentIndividual = individuals.at(i);
        if (i != number && 
            distance(currentIndividual) < SIGHT && 
            !currentIndividual.isNearBorder(BORDER) && 
            y - currentIndividual.getY() >= 0) {
            closeIndividuals.push_back(individuals.at(i));
        }
    }

    // Séparation
    int newTooCloseHeading = heading;
    int nTooCloseIndividuals = 0;
    for (int i = 0; i < closeIndividuals.size(); i++) {
        Individual currentCloseIndividual = closeIndividuals.at(i);
        int d = distance(currentCloseIndividual);
        if (d < DREPULSION && d > 0) {
            newTooCloseHeading += acos((x - currentCloseIndividual.getX()) / d) * 360 / (2 * M_PI);
            nTooCloseIndividuals++;
        }
    }

    // Cohérence
    float newTooFarX = 0;
    float newTooFarY = 0;
    int nTooFarIndividuals = 0;
    for (int i = 0; i < closeIndividuals.size(); i++) {
        Individual currentCloseIndividual = closeIndividuals.at(i);
        int d = distance(currentCloseIndividual);
        if (d > DATTRACTION) {
            newTooFarX += currentCloseIndividual.getX();
            newTooFarY += currentCloseIndividual.getY();
            nTooFarIndividuals++;
        }
    }

    if (nTooCloseIndividuals != 0) {
        heading = int(float(newTooCloseHeading) / nTooCloseIndividuals) % 360;
    } else if (nTooFarIndividuals != 0) {
        // Cohérence
        newTooFarX = newTooFarX / nTooFarIndividuals;
        newTooFarY = newTooFarY / nTooFarIndividuals;
        int dToDest = int(sqrt(pow(x - newTooFarX, 2) + pow(y - newTooFarY, 2)));
        if (dToDest != 0) {
            heading = int(asin((newTooFarY - y) / dToDest) * 360 / (2 * M_PI)) % 360;
        }
    } else {
        // Alignement
        for (int i = 0; i < closeIndividuals.size(); i++) {
            Individual currentCloseIndividual = closeIndividuals.at(i);
            int d = distance(currentCloseIndividual);
            if (d < DATTRACTION && d > DREPULSION) {
                heading += currentCloseIndividual.getHeading();
            }
        }
        heading = int(float(heading) / (closeIndividuals.size() + 1)) % 360;
    }
    heading = (heading + (std::rand() % 51) - 25) % 360;

    int newX = x + int(cos(heading * 2 * M_PI / 360) * 3);
    int newY = y + int(sin(heading * 2 * M_PI / 360) * 3);

    x = newX >= 0 ? newX : width;
    x = x <= width ? x : 0;

    y = newY >= 0 ? newY : height;
    y = y <= height ? y : 0;
}

int Individual::distance(Individual otherIndividual) {
    return sqrt(pow(x - otherIndividual.getX(), 2) + pow(y - otherIndividual.getY(), 2));
}

bool Individual::isNearBorder(int border) {
    return x <= border || y <= border || x >= (width - border) || y >= (height - border);
}

bool Individual::onWhichSideIsIndividual(Individual otherIndividual) {
    return (x - otherIndividual.getX()) >= 0;
}