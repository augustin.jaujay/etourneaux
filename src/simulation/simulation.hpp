#ifndef SIMULATION_HPP_INCLUDED
#define SIMULATION_HPP_INCLUDED

#include <vector>
#include "../individual/individual.hpp"

class Simulation {
    private:

    int width;
    int height;
    std::vector<Individual> individuals;

    void updateIndividualsWindowSize();

    public:

    Simulation(int, int, int);
    int getWidth();
    int getHeight();
    void setSize(int w, int h);
    std::vector<Individual> getIndividuals();
    void step();
};

#endif // SIMULATION_HPP_INCLUDED