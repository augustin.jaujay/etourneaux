#include <iostream>
#include "simulation.hpp"

Simulation::Simulation(int n, int argWidth, int argHeight) : width(argWidth), height(argHeight) {
    for (int i = 0; i < n; i++) {
        Individual indiv(i, width, height);
        individuals.push_back(indiv);
    }
}

void Simulation::updateIndividualsWindowSize() {
    for (int i = 0; i < individuals.size(); i++) {
        individuals.at(i).setSize(width, height);
    }
}

int Simulation::getWidth() {
    return width;
}

int Simulation::getHeight() {
    return height;
}

void Simulation::setSize(int w, int h) {
    width = w;
    height = h;
    updateIndividualsWindowSize();
}

std::vector<Individual> Simulation::getIndividuals() {
    return individuals;
}

void Simulation::step() {
    for (int i = 0; i < individuals.size(); i++) {
        individuals.at(i).step(individuals);
    }
}