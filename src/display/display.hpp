#ifndef DISPLAY_HPP_INCLUDED
#define DISPLAY_HPP_INCLUDED

#include "SDL2/SDL.h"
#include "../simulation/simulation.hpp"

class Display {
    private:

    SDL_Window* window;
    SDL_Renderer* renderer;
    Simulation* displaySimu;
    SDL_Event e;

    public:

    Display(Simulation* simulation);
    bool handleEvent();
    void update();
    ~Display();
};

#endif // DISPLAY_HPP_INCLUDED