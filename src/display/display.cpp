#include <iostream>
#include "display.hpp"

Display::Display(Simulation *simulation) {
    displaySimu = simulation;
    window = NULL;
    renderer = NULL;

    int flags = SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE;
    window = SDL_CreateWindow("Modélisation Étourneaux", 
        SDL_WINDOWPOS_CENTERED, 
        SDL_WINDOWPOS_CENTERED, 
        displaySimu->getWidth(), 
        displaySimu->getHeight(), flags);

    if (window == NULL) {
        std::cerr << "Unable to create window : " << SDL_GetError() << std::endl;
    }

    renderer = SDL_CreateRenderer(window, -1, 0);

    if (renderer == NULL) {
        std::cerr << "Unable to create renderer : " << SDL_GetError() << std::endl;
    }
}

bool Display::handleEvent() {
    bool quit = false;

    while (SDL_PollEvent(&e)){
        if (e.type == SDL_QUIT){
            quit = true;
        }
        if (e.type == SDL_KEYDOWN){
            quit = true;
        }
        if (e.type == SDL_WINDOWEVENT && e.window.event == SDL_WINDOWEVENT_RESIZED) {
            int w, h;
            SDL_GetWindowSize(window, &w, &h);
            displaySimu->setSize(w, h);
        }
    }

    return quit;
}

void Display::update() {
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);

    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);

    std::vector<Individual> individualsTable = displaySimu->getIndividuals();
    for (int i = 0; i < individualsTable.size(); i++) {
        Individual currentIndividual = individualsTable.at(i);
        SDL_Rect rect = {currentIndividual.getX() - 1, currentIndividual.getY() - 1, 3, 3};
        SDL_RenderDrawRect(renderer, &rect);
    }

    SDL_RenderPresent(renderer);
}

Display::~Display() {
    if (NULL != renderer) {
        SDL_DestroyRenderer(renderer);
    }
    if (NULL != window) {
        SDL_DestroyWindow(window);
    }
}