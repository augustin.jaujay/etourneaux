#include <iostream>
#include "SDL2/SDL.h"
#include "display/display.hpp"
#include "individual/individual.hpp"
#include "simulation/simulation.hpp"

#define NB_INDIVIDUALS 50
#define WIDTH 1000
#define HEIGHT 1000

int main() {
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        std::cerr << "Unable to initialize SDL2 : " << SDL_GetError() << std::endl;

        exit(EXIT_FAILURE);
    }

    Simulation mainSimulation(NB_INDIVIDUALS, WIDTH, HEIGHT);
    Display mainDisplay(&mainSimulation);
        
    bool quit = false;
    while (!quit){
        quit = mainDisplay.handleEvent();

        SDL_Delay(30);

        mainSimulation.step();
        mainDisplay.update();
    }

    mainDisplay.~Display();
    SDL_Quit();

    return 0;
}